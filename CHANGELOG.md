#### 版本迭代

- v1.0.1
  1. api8升级到api9
  2.添加sanitize-html依赖库，完善jsoup功能

- v1.0.0
- 已实现功能
  1. 从字符串解析HTML
  2. 提取CSS
  3. 解析自定义HTML标签
  4. 获取HTML属性
  5. 从字文件解析HTML
  6. HTML标签补充
  7. 从URL解析HTML
  8. HTML清除XSS