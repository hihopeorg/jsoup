/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ParserHtmlString from 'parser-html-json'
import fileio from '@ohos.fileio';
import http from '@ohos.net.http';

export default class Jsoup {
    public static connect(url: string, callBack: any, method?: http.RequestMethod, contentType?: string, connectTimeout?: number, readTimeout?: number) {
        const requestM = !!!method ? http.RequestMethod.GET : method
        let httpRequest = http.createHttp()
        httpRequest.request(url,
            {
                method: requestM,
                header: {
                    'Content-Type': !!!contentType ? 'application/json' : contentType
                },
                readTimeout: !!!readTimeout ? 50000 : readTimeout,
                connectTimeout: !!!connectTimeout ? 50000 : connectTimeout,
            }, (err, data) => {
                callBack(err, data.result.toString())
            });
    }

    public static parseCSS(html: string): string{
        const parser = new ParserHtmlString(html)
        return JSON.stringify(parser.getClassStyleJson());
    }

    public static parseHtmlFromFile(stream: fileio.Stream, htmlLength: number): string {
        let buf = new ArrayBuffer(htmlLength)
        stream.readSync(buf, {
            offset: 0, length: htmlLength, position: 0
        })
        let readText = String.fromCharCode.apply(null, new Uint8Array(buf))
        return readText
    }
}

