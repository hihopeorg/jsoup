/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class Cleaner {
    public TO_CONFIGURE = ['html', 'js', 'jsAttr', 'uri', 'jsObj', 'css', 'style'];
    public QUOT = /\x22/g; // "
    public APOS = /\x27/g; // '
    public AST = /\*/g;
    public TILDE = /~/g;
    public BANG = /!/g;
    public LPAREN = /\(/g;
    public RPAREN = /\)/g;
    public CDATA_CLOSE = /\]\](?:>|\\x3E|\\u003E)/gi;

    // Matches alphanum plus ",._-" & unicode.
    // ESAPI doesn't consider "-" safe, but we do. It's both URI and HTML safe.
    public JS_NOT_WHITELISTED = /[^,\-\.0-9A-Z_a-z]/g;

    // add on '":\[]{}', which are necessary JSON metacharacters
    public JSON_NOT_WHITELISTED = /[^\x22,\-\.0-9:A-Z\[\x5C\]_a-z{}]/g;

    // Control characters that get converted to spaces.
    public HTML_CONTROL = /[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F]/g;

    // Matches alphanum plus allowable whitespace, ",._-", and unicode.
    // NO-BREAK SPACE U+00A0 is fine since it's "whitespace".
    public HTML_NOT_WHITELISTED = /[^\t\n\v\f\r ,\.0-9A-Z_a-z\-\u00A0-\uFFFF]/g;

    // Matches alphanum and UTF-16 surrogate pairs (i.e. U+10000 and higher). The
    // rest of Unicode is deliberately absent in order to prevent charset encoding
    // issues.
    public CSS_NOT_WHITELISTED = /[^a-zA-Z0-9\uD800-\uDFFF]/g;
    private static ATTRIBUTE_CONFIG = ['onclick', 'ondblclick', 'onmousedown',
    'onerror', 'onload', 'onchange', 'onfocus', 'onsubmit', 'oninput', 'onreset']
    private static URL_CONFIGS = ['src', 'href', 'background-url', 'url']
    private static PROTOCOL_CONFIGS = ['http', 'https', 'ftp', 'mailto']
    private static FILTER_TAGS = ['javascript', 'script', 'alert', 'Cookies', 'redirect']
    private static REPLACE_JS_EVENT = ['rel', 'nofollow']
    private static REPLACE_JS_SCRIPT = ['noaction']
    private static JS_CONTENT = ['text']

    private html(val: string): string {
        var str = String(val);
        str = str.replace(this.HTML_CONTROL, ' ');
        return str.replace(this.HTML_NOT_WHITELISTED, this.matchHtml(str));
    }

    private matchHtml(match: string) {
        var code = match.charCodeAt(0);
        switch (code) {
        // folks expect these "nice" entities:
            case 0x22:
                return '&quot;';
            case 0x26:
                return '&amp;';
            case 0x3C:
                return '&lt;';
            case 0x3E:
                return '&gt;';

            default:
            // optimize for size:
                if (code < 100) {
                    var dec = code.toString(10);
                    return '&#' + dec + ';';
                } else {
                    // XXX: this doesn't produce strictly valid entities for code-points
                    // requiring a UTF-16 surrogate pair. However, browsers are generally
                    // tolerant of this. Surrogate pairs are currently in the whitelist
                    // defined via HTML_NOT_WHITELISTED.
                    var hex = code.toString(16).toUpperCase();
                    return '&#x' + hex + ';';
                }
        }
    }

    public static clean(input: any[]): any[]{
        if (!!!input) {
            return
        }
        var temDom = input;
        // dom ->body
        for (var index = 0; index < temDom.length; index++) {
            var element = temDom[index];
            var childGroup: any[] = element.children
            this.searchChild(childGroup)
        }
        return input;
    }

    private static searchChild(childGroup: any[]) {
        var temGroup = childGroup;
        for (var childIndex = 0; childIndex < temGroup.length; childIndex++) {
            var child = childGroup[childIndex];
            this.validateAttr(child)
            this.validateContent(child)
            this.searchChild(child.children)
        }
    }

    private static validateAttr(child: any) {
        if (!!!child) {
            return;
        }
        if (child.hasOwnProperty('attr')) {
            for (var attrIndex = 0; attrIndex < Cleaner.URL_CONFIGS.length; attrIndex++) {
                var element = Cleaner.URL_CONFIGS[attrIndex];
                for (var urlIndex = 0; urlIndex < Cleaner.PROTOCOL_CONFIGS.length; urlIndex++) {
                    if (child.attr[element] != null && String(child.attr[element]).indexOf(Cleaner.PROTOCOL_CONFIGS[urlIndex]) != -1) {
                        for (var filterIndex = 0; filterIndex < Cleaner.FILTER_TAGS.length; filterIndex++) {
                            child.attr[element] = String(child.attr[element]).replace(Cleaner.FILTER_TAGS[filterIndex], Cleaner.REPLACE_JS_EVENT[1])
                        }
                    }
                }
            }
            for (var eventIndex = 0;eventIndex < Cleaner.ATTRIBUTE_CONFIG.length; eventIndex++) {
                var key = Cleaner.ATTRIBUTE_CONFIG[eventIndex]
                for (var eventValue = 0; eventValue < Cleaner.FILTER_TAGS.length; eventValue++) {
                    if (key in child.attr && String(child.attr[key]).indexOf(Cleaner.FILTER_TAGS[eventValue]) != -1) {
                        child.attr[key] = Cleaner.REPLACE_JS_EVENT[1]
                        child.attr[Cleaner.REPLACE_JS_EVENT[0]] = child.attr[key];
                        delete child.attr[key];
                    }
                }
            }
        }
    }

    private static validateContent(child: any) {
        if (!!!child) {
            return;
        }
        if (child.hasOwnProperty('text')) {
            for (var index = 0; index < Cleaner.FILTER_TAGS.length; index++) {
                if (!child[Cleaner.JS_CONTENT[0]] && String(child[Cleaner.JS_CONTENT[0]]).indexOf(Cleaner.FILTER_TAGS[index]) != -1) {
                    child[Cleaner.JS_CONTENT[0]] = JSON.stringify(child[Cleaner.JS_CONTENT[0]]).replace(Cleaner.FILTER_TAGS[index], Cleaner.REPLACE_JS_SCRIPT[0])
                }
            }
        }
    }

    public static addFilterTags(...filterTagName: string[]) {
        if (!!!filterTagName) {
            return;
        }
        for (var index = 0; index < Cleaner.FILTER_TAGS.length; index++) {
            for (var attrIndex = 0; attrIndex < filterTagName.length; attrIndex++) {
                if (Cleaner.FILTER_TAGS[index] != filterTagName[attrIndex]) {
                    Cleaner.FILTER_TAGS.push(filterTagName[attrIndex])
                }
            }
        }
    }

    public static addAttribute(...attributeName: string[]) {
        if (!!!attributeName) {
            return;
        }
        for (var index = 0; index < Cleaner.ATTRIBUTE_CONFIG.length; index++) {
            for (var attrIndex = 0; attrIndex < attributeName.length; attrIndex++) {
                if (Cleaner.ATTRIBUTE_CONFIG[index] != attributeName[attrIndex]) {
                    Cleaner.ATTRIBUTE_CONFIG.push(attributeName[attrIndex])
                }
            }
        }
    }

    public static removeAttribute(attributeName: string) {
        if (!!!attributeName) {
            return;
        }
        for (var index = 0; index < Cleaner.ATTRIBUTE_CONFIG.length; index++) {
            var searchIndex = Cleaner.ATTRIBUTE_CONFIG.indexOf(attributeName)
            if (searchIndex != -1) {
                Cleaner.ATTRIBUTE_CONFIG.splice(searchIndex, 1)
            }
        }
    }

    public static addProtocols(...protocolName: string[]) {
        if (!!!protocolName) {
            return;
        }
        for (var index = 0; index < Cleaner.PROTOCOL_CONFIGS.length; index++) {
            for (var attrIndex = 0; attrIndex < protocolName.length; attrIndex++) {
                if (Cleaner.PROTOCOL_CONFIGS[index] != protocolName[attrIndex]) {
                    Cleaner.PROTOCOL_CONFIGS.push(protocolName[attrIndex])
                }
            }
        }
    }

    public static removeProtocol(protocolName: string) {
        if (!!!protocolName) {
            return;
        }
        for (var index = 0; index < Cleaner.PROTOCOL_CONFIGS.length; index++) {
            var searchIndex = Cleaner.PROTOCOL_CONFIGS.indexOf(protocolName)
            if (searchIndex != -1) {
                Cleaner.PROTOCOL_CONFIGS.splice(searchIndex, 1)
            }
        }
    }

    /**
     * Backslash-encoding for a single character in JavaScript contexts.
     * @param {string} charStr single-character string.
     * @return {string} backslash escaped character.
     * @private
     */
    public jsSlashEncoder(charStr: string) {
        var code = charStr.charCodeAt(0);
        var hex = code.toString(16).toUpperCase();
        if (code < 0x80) { // ASCII
            if (hex.length === 1) {
                return '\\x0' + hex;
            } else {
                return '\\x' + hex;
            }
        } else { // Unicode
            switch (hex.length) {
                case 2:
                    return '\\u00' + hex;
                case 3:
                    return '\\u0' + hex;
                case 4:
                    return '\\u' + hex;
                default:
                // charCodeAt() JS shouldn't return code > 0xFFFF, and only four hex
                // digits can be encoded via `\u`-encoding, so return REPLACEMENT
                // CHARACTER U+FFFD.
                    return '\\uFFFD';
            }
        }
    }

    private js(val: string) {
        var str = String(val);
        return str.replace(this.JS_NOT_WHITELISTED, this.jsSlashEncoder(str));
    }

    /**
    * Encodes values embedded in HTML scripting attributes.
    *
    * See jsAttr(value) in README.md for full documentation.
    *
    * @name jsAttr
    * @param {any} val will be converted to a String prior to encoding
    * @return {string} the encoded string
    */
    private jsAttr(val: string) {
        return this.html(this.js(val));
    }

    /**
     * Percent-encodes unsafe characters in URIs.
     *
     * See uri(value) in README.md for full documentation.
     *
     * @name uri
     * @param {any} val will be converted to a String prior to encoding
     * @return {string} the percent-encoded string
     */
    private uri(val: string) {
        // encodeURIComponent() is well-standardized across browsers and it handles
        // UTF-8 natively.  It will not encode "~!*()'", so need to replace those here.
        // encodeURIComponent also won't encode ".-_", but those are known-safe.
        //
        // IE does not always encode '"' to '%27':
        // http://blog.imperva.com/2012/01/ie-bug-exposes-its-users-to-xss-attacks-.html
        var encode = encodeURIComponent(String(val));
        return encode
            .replace(this.BANG, '%21')
            .replace(this.QUOT, '%27')
            .replace(this.APOS, '%27')
            .replace(this.LPAREN, '%28')
            .replace(this.RPAREN, '%29')
            .replace(this.AST, '%2A')
            .replace(this.TILDE, '%7E');
    }

    /**
     * Encodes previously generated JSON ensuring unsafe characters in string
     * literals are backslash-escaped.
     *
     * See json(value) in README.md for full documentation.
     *
     * @name json
     * @param {string} val
     * @return {string} the backslash-encoded string
     */
    private json(val: string) {
        var str = String(val);
        return str.replace(this.JSON_NOT_WHITELISTED, this.jsSlashEncoder(str))
            // prevent breaking out of CDATA context.  Escaping < below is sufficient
            // to prevent opening a CDATA context.
            .replace(this.CDATA_CLOSE, '\\x5D\\x5D\\x3E');
    };

    /**
     * Encodes an object as JSON, but with unsafe characters in string literals
     * backslash-escaped.
     *
     * See jsObj(value) in README.md for full documentation.
     *
     * @name jsObj
     * @param {any} val
     * @return {string} the JSON- and backslash-encoded string
     */
    public jsObj(val: string): string {
        return this.json(JSON.stringify(val));
    }

    /**
     * Encodes values for safe embedding in CSS context.
     *
     * See css(value) in README.md for full documentation.
     *
     * @name css
     * @param {any} val
     * @return {string} the backslash-encoded string
     */
    public css(val: string): string {
        var str = String(val);
        return str.replace(this.CSS_NOT_WHITELISTED, this.cssMatch(str));
    }

    private cssMatch(match: string) {
        var code = match.charCodeAt(0);
        if (code === 0) {
            return '\\fffd '; // REPLACEMENT CHARACTER U+FFFD
        } else {
            var hex = code.toString(16).toLowerCase();
            return '\\' + hex + ' ';
        }
    }

    /**
     * Encodes values for safe embedding in HTML style attribute context.
     *
     * See style(value) in README.md for full documentation.
     *
     * @name style
     * @param {any} val
     * @return {string} the entity- and backslash-encoded string
     */
    public style(val: string): string{
        return this.html(this.css(val));
    };
}