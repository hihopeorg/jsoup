/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Tag from './tag'

export default class LabelSupplement {
    private static template: string = '';

    public  static process(input: string): void {

        var tags: Array<Tag> = new Array<Tag>()

        var lastTag: Tag
        let splits: string[]
        if (input.indexOf("\n") != -1) {
            splits = input.split("\n", input.length)
        }
        if (input.indexOf("\r\n") != -1) {
            splits = input.split("\r\n", input.length)
        }
        splits.forEach((value, index, array) => {
            var indentCount: number = 0;
            var tagName: string[] = value.split("\t")
            tagName.forEach((childValue, index, array) => {
                if (childValue == "") indentCount++;
            })
            var tag: Tag = new Tag();
            tag.setIndentCount(indentCount)
            var attributes: string[];
            attributes = tagName[tagName.length-1].split(" ")
            tag.setName(attributes[0])
            attributes.forEach((attr) => {
                var attrs: string[] = attr.split(":")
                if (attrs.length > 0) {
                    tag.setAttributes(new Map().set(attrs[0], attrs[1]))
                }
            })
            LabelSupplement.analyzeTagStructure(tags, tag, lastTag)
            lastTag = tag
        })

        LabelSupplement.show(tags)
    }

    private static analyzeTagStructure(tags: Array<Tag>, tag: Tag, lastTag: Tag): void {
        if (lastTag == null) {
            tags.push(tag)
        } else {
            let indentCount = tag.getIndentCount()
            let lastIndentCount = lastTag.getIndentCount()

            if (indentCount == lastIndentCount) {
                tag.setParent(lastTag.getParent())
                lastTag.getParent().getChildren().push(tag)

            } else if (indentCount > lastIndentCount) {
                tag.setParent(lastTag)
                lastTag.getChildren().push(tag)

            } else {
                var tmpParentTag = lastTag.getParent()
                while (true) {
                    if (tmpParentTag == null) {
                        tags.push(tag)
                        break
                    } else if (indentCount > tmpParentTag.getIndentCount()) {
                        tag.setParent(tmpParentTag)
                        tmpParentTag.getChildren().push(tag)
                        break
                    }
                    tmpParentTag = tmpParentTag.getParent()
                }
            }
        }
    }

    private static show(tags: Array<Tag>): void {
        tags.forEach((it: Tag, index, array) => {
            LabelSupplement.showTag(it)

            if (it.getChildren().length > 0) {
                LabelSupplement.show(it.getChildren())
                console.info("</" + it.getName() + ">")
                LabelSupplement.template += "</" + it.getName() + ">";
            }
        })
    }

    private static showTag(tag: Tag): void {
        var a = ''
        tag.getAttributes().forEach((value, key, map) => {
            if (!!!key || !!!value) {
                return;
            }
            a += (key + ":" + value)
        })
        if (!!!tag.getAttributes().get("")) {

            if (a != '') {
                LabelSupplement.template += "<" + tag.getName() + " " + a + ">";
            } else {
                LabelSupplement.template += "<" + tag.getName() + a + ">";
            }

            console.info("<" + tag.getName() + "" + a + ">")

        } else {
            LabelSupplement.template += "<" + tag.getName() + a + ">" + tag.getAttributes().get("")

            console.info("<" + tag.getName() + " " + a + ">" + tag.getAttributes().get(""))
        }

        if (tag.getChildren().length == 0) {
            LabelSupplement.template += "</" + tag.getName() + ">"
            console.info("</" + tag.getName() + ">")
        }
    }

    public static getTemplate(): string {
        return LabelSupplement.template;
    }
}