/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default class Tag {
  private name: string;
  private indentCount: number;
  private attributes: Map<string, string>;
  private children: Array<Tag>;
  private parent: Tag;

  public constructor() {
    this.attributes = new Map<string, string>();
    this.children = new Array<Tag>();
  }

  public getName(): string {
    return this.name;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public getIndentCount(): number {
    return this.indentCount;
  }

  public setIndentCount(indentCount: number): void {
    this.indentCount = indentCount;
  }

  public getAttributes(): Map<string, string> {
    return this.attributes;
  }

  public setAttributes(attributes: Map<string, string>): void {
    this.attributes = attributes;
  }

  public getChildren(): Array<Tag> {
    return this.children;
  }

  public setChildren(children: Array<Tag>): void {
    this.children = children;
  }

  public getParent(): Tag {
    return this.parent;
  }

  public setParent(parent: Tag): void {
    this.parent = parent;
  }

  public toString(): string {
    return "Tag{" +
    "name='" + this.name + '\'' +
    ", indentCount=" + this.indentCount +
    ", attributes=" + this.attributes +
    ", children=" + this.children +
    ", parent=" + this.parent +
    '}';
  }
}