# jsoup

## 简介
> jsoup支持根据URL、HTML文件、HTML字符串解析HTML；支持操作HTML元素、属性、文本；支持DOM遍历或CSS选择器查找和提取数据。

![preview.gif](preview/preview.gif)

## 下载安装
```shell
npm install @ohos/jsoup --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明
1. 引入依赖
 ```
   import { Jsoup } from '@ohos/jsoup'
   import { SanitizeHtml } from '@ohos/jsoup'
   import { Parser } from '@ohos/jsoup'
   import { DomHandler, Document } from '@ohos/jsoup'
   import { DomUtils } from '@ohos/jsoup'
 ```
2. 解析HTML
 ```
 const html = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    .tagh1{
        background-color: aquamarine;
        color:'blue';
    }
    .one-div{
        line-height: 30px;
    }
</style>
<body>
    <h1 class="tagh1">
        kkkk
        <p>hhhhh</p>
    </h1>
    <div style="color:red; height:100px;" class="one-div">cshi</div>
    <img src="https:baidu.com" alt="wwww"/>
    <p>wjdwekfe>>>>></p>
    <em>dsjfw<<<<<p
    <div>dksfmjk</div>
    owqkdo</em>
</body>
</html>
`
const parser = new Parser.Parser({
  onopentag(name, attributes) {
    console.info(`jsoup onopentag name --> ${name}  attributes --> ${attributes}`)
  },
  ontext(text) {
    console.info("jsoup text -->", text);
  },
  onopentagname(name) {
    console.info("jsoup tagName -->", name);
  },
  onattribute(name, value) {
    console.info(`jsoup attribName name --> ${name}  value --> ${value}`)
  },
  onclosetag(tagname) {
    console.info("jsoup closeTag --> ", tagname);
  },
});
parser.write(html);
parser.end();
 ```

## 接口说明
1. 解析字符串类型的HTML
   `const parser = new Parser.Parser()`
   `parser.write()`
2. 提取CSS
   `Jsoup.parseCSS()`
3. 解析文件类型的HTML
   `Jsoup.parseHtmlFromFile()`
4. 解析url类型的HTML
   `Jsoup.connect()`

## 兼容性
支持 OpenHarmony API version 9 及以上版本。

## 目录结构
````
|---- jsoup  
|     |---- entry  # 示例代码文件夹
|        |----src
|           |----addTag.ets
|           |----index.ets
|     |---- jsoup  # jsoup库文件夹
|	    |----src
          |----main
              |----ets
                  |----common 模板
                  |----Cleaner.ts #html clean
                  |----Jsoup.ts #html解析
|           |---- index.ts  # 对外接口
|     |---- README.md  # 安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/jsoup/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/jsoup/pulls) 。

## 开源协议
本项目基于 [MIT](https://gitee.com/openharmony-sig/jsoup/blob/master/LICENSE) ，请自由地享受和参与开源。
